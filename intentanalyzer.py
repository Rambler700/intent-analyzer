# -*- coding: utf-8 -*-
"""
Created on Sun Aug  4 18:21:03 2019

@author: enovi
"""

import nltk
# Intent Analyzer

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Intent Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_text(article_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_text(article_text):
    
    know     = 0
    do       = 0
    location = 0
    site     = 0
    keywords = 0
    
    know_list = ['advice', 'blog', 'forum', 'guide', 'how', 'instruction', 'journal', 'know',
                 'learn', 'manual', 'need', 'opinion', 'overview', 'paper', 'post', 'study',
                 'survey', 'tip', 'view', 'way', 'what', 'when', 'who', 'why']
    
    do_list = ['acquire', 'best', 'bid', 'buy', 'call', 'compare', 'contact', 'consign', 'coupon',
               'deal', 'discount', 'estimate', 'fax', 'finance', 'lease', 'loan', 'obtain', 'offer',
               'phone', 'popular', 'price', 'proposal', 'purchase', 'rent', 'review', 'request',
               'sale', 'sell', 'shipping', 'subscribe', 'top', 'trade', 'transaction', 'value']
    
    location_list = ['address', 'around', 'bike', 'building', 'bus', 'city', 'close', 'direction',
                     'drive', 'local', 'location', 'mall', 'near', 'office', 'outlet', 'retail',
                     'ride', 'store', 'suite', 'town', 'tram', 'walk', 'where', 'within']
    
    site_list = ['login', 'password', 'page', 'site']
    
    article_text = article_text.lower()
    article_text = article_text.split(' ')
    
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
    
    for word in know_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in do_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in location_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in site_list:
        word = nltk.PorterStemmer().stem(word) 

    for word in article_text:        
        if word in know_list:
            know += 1
        if word in do_list:
            do += 1
        if word in location_list:
            location += 1
        if word in site_list:
            site += 1
        keywords += 1
    
    if keywords > 0:    
        print('Information:',know/keywords,'%')
        print('Transaction:',do/keywords,'%')
        print('Location:   ',location/keywords,'%')
        print('Navigation: ',site/keywords,'%')    
    else:
        print('No intent keywords detected.')        
main()